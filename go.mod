go 1.13

module gitlab.com/go-grammar-rus/words

require (
	github.com/amarin/binutils v0.2.1
	gitlab.com/go-grammar-rus/grammemes v0.3.1
	gitlab.com/go-grammar-rus/text v0.1.3
	golang.org/x/text v0.3.2
)
