package words

// Package defines its own error implementation to distinct error types when required.

import (
	"fmt"
)

// ErrorWords implements error interface and implements internal words package error.
type ErrorWords struct {
	source  string
	wrapped error
}

// Unwrap returns wrapped error if present.
func (e *ErrorWords) Unwrap() error {
	return e.wrapped
}

// Error returns error description string. Implements error interface.
func (e *ErrorWords) Error() string {
	if e.wrapped != nil {
		return e.source + ": " + e.wrapped.Error()
	}

	return e.source
}

// WrapIntoErrorWords creates new ErrorWords instance wrapping underlying.
// Takes error to wrap, format string and formatting arguments.
func WrapIntoErrorWords(wrap error, format string, args ...interface{}) *ErrorWords {
	return &ErrorWords{source: fmt.Sprintf(format, args...), wrapped: wrap}
}

// NewErrorWords создаёт creates new ErrorWords instance.
// Takes format string and formatting arguments.
func NewErrorWords(format string, args ...interface{}) *ErrorWords {
	return &ErrorWords{source: fmt.Sprintf(format, args...), wrapped: nil}
}
